#!/usr/bin/env python3

"""
This module automates common AR Altek Stereo Cam operations

Copyright 2019 Amazon.com, Inc. or its affiliatess. All rights reserved.
"""

import os
import sys
import configparser
import sc_processing as sc
import libs.alcam as al
import libs.common_utils as cu


REMOTE_USERNAME = "root"
REMOTE_IP = "172.17.1.10"

config_dict = {'default': ['capture_file'],
               'capture': ['new_capture',
                           'path',
                           'format',
                           'width',
                           'height',
                           'rate',
                           'count',
                           'period',
                           'message',
                           'test_pattern'],
               'sc_processing': ['run_processing',
                                 'frame_id',
                                 'all_proc',
                                 'raw',
                                 'disparity',
                                 'depth',
                                 'confidence',
                                 'pts_csv',
                                 'pts_ply',
                                 'x_min',
                                 'x_max',
                                 'y_min',
                                 'y_max',
                                 'z_min',
                                 'z_max']}


def verify_config(config):
    """ Verify the config parsed from config file
        Args:
            config: config parsed from config file
        Returns:
            0 on success, -1 on failure
    """
    for key in config_dict:
        if key in config:
            for item in config_dict[key]:
                if item not in config[key]:
                    print("[{}] {} missing from the config".format(key, item))
                    return -1
        else:
            print("[{}] missing from the config".format(key))
            return -1
    return 0


def str_to_int(str):
    """ Convert string to int
        Args:
            str: string to be converted
    """
    try:
        value = int(str)
    except ValueError:
        return None
    return value


def main():
    if len(sys.argv) < 2:
        print("Please specify the config file path. e.g. ./alcam_tool.py alcam_tool_config.ini")
        return

    config_file = sys.argv[1]
    if not os.path.isfile(config_file):
        print("Error: input config file does not exist or is not a file:", config_file)
        return

    # parse config file arguments
    print("\n>>>> Parse config file: {}".format(config_file))
    config = configparser.ConfigParser()
    config.sections()
    config.read(config_file)

    if verify_config(config) != 0:
        return

    if (config["capture"]["new_capture"] == "y"):
        # run capture command on remote client
        print("\n>>>> Capture from camera")
        cam = al.AlcamRemote(REMOTE_IP, REMOTE_USERNAME, "/dev/video-ods-cam")
        remote_output_file = "/tmp/" + os.path.basename(config['default']['capture_file'])
        settings = config['capture']
        test_pattern = int(settings['test_pattern']) == 1
        errcode, cmd_out, cmd_err = cam.capture(remote_output_file,
                                                settings['format'],
                                                test_pattern,
                                                settings['width'],
                                                settings['height'],
                                                settings['rate'],
                                                settings['count'],
                                                settings['period'],
                                                settings['message'])
        if errcode != 0:
            print(cmd_out, cmd_err)
            return
        print(cmd_out)

        # copy the capture output from remote client
        sc_file = config['default']['capture_file']
        print("\n>>>> Copy the capture output to local {}".format(sc_file))
        errcode = cu.copy_from_remote(REMOTE_USERNAME, REMOTE_IP, remote_output_file, sc_file)
        if errcode != 0:
            return
    else:
        print("\n>>>> SKIP: Capture from camera")

    # process the capture output
    config_proc = config["sc_processing"]
    if config_proc["run_processing"] == "y":
        print("\n>>>> Process the capture output")
        sc.process(config['default']['capture_file'],
                   str_to_int(config_proc['frame_id']),
                   config_proc['all_proc'],
                   config_proc['raw'],
                   config_proc['disparity'],
                   config_proc['depth'],
                   config_proc['confidence'],
                   config_proc['pts_csv'],
                   config_proc['pts_ply'],
                   [str_to_int(config_proc['x_min']),
                    str_to_int(config_proc['x_max']),
                    str_to_int(config_proc['y_min']),
                    str_to_int(config_proc['y_max']),
                    str_to_int(config_proc['z_min']),
                    str_to_int(config_proc['z_max'])])
    else:
        print("\n>>>> SKIP: Process the capture output")


if __name__ == "__main__":
    main()
