#!/usr/bin/env python3

"""
This module contains API to process AR altek stereo camera data

Copyright 2019 Amazon.com, Inc. or its affiliatess. All rights reserved.
"""
import argparse
import textwrap
import os

import cv2 as cv
import numpy as np

import libs.common_utils as cu
import libs.alcam as al

IMG_TYPE_GENERIC = "generic"
IMG_TYPE_AL_DISPARITY = "al_disparity"
IMG_TYPE_AL_MAINSUB = "al_mainsub"


# Ensure the given parameters are consistent/will produce a correct output
def validate_parameters(binary_input, height, width, type, bytes_per_element):
    if binary_input is None:
        print("ERROR: missing input stream file")
        return False
    elif not os.path.isfile(binary_input):
        print("ERROR: input file does not exist or is not a file:",
              binary_input)
        return False

    # Check if the resoulution is valid for the type
    if type == IMG_TYPE_AL_DISPARITY:
        if not((width == 1280 and height == 720)
           or (width == 640 and height == 360)
           or (width == 320 and height == 180)):
            print("ERROR:", width, "x", height,
                  "is not a supported disparity resolution")
            return False

    elif type == IMG_TYPE_AL_MAINSUB:
        if not(width == 1296 and height == 1600):
            print("ERROR:", width, "x", height,
                  "is not a supported mainsub resolution")
            return False

    # Check size of file
    expected_size = width * height * bytes_per_element
    actual_size = os.path.getsize(binary_input)
    if expected_size != actual_size:
        print("ERROR: unexpected file size: ", expected_size,
              " expected_size ", actual_size, "acual_size")
        return False

    return True


def process(binary_input, height, width, type, bytes_per_element):
    # Parse the type of the input file
    if type is None:
        type = IMG_TYPE_GENERIC
        scale = 1
        mask = 2**(bytes_per_element*8) - 1
    elif type == IMG_TYPE_AL_DISPARITY:
        bytes_per_element = 2
        scale = 8
        mask = 0x3ff
    elif type == IMG_TYPE_AL_MAINSUB:
        bytes_per_element = 2
        scale = 1
        mask = 2**(bytes_per_element*8) - 1
    else:
        print("\"{}\" is not a valid type. Must be \"{}\" or \"{}\""
              .format(type, IMG_TYPE_AL_DISPARITY, IMG_TYPE_AL_MAINSUB),
              "if included at all.")
        return

    # Check that the input parameters are valid
    if not validate_parameters(binary_input, height, width, type,
                               bytes_per_element):
        return

    # Open the input_file
    with open(binary_input, 'rb') as binary_data:
        # Convert binary to pixel array (assumes little endian)
        pixel_map = [[0 for x in range(width)] for y in range(height)]
        for i in range(height):
            for j in range(width):
                pixel = int.from_bytes(binary_data.read(bytes_per_element),
                                       'little')
                pixel_map[i][j] = (pixel & mask) / scale

    # Create file prefix
    output_prefix = os.path.join(os.path.dirname(binary_input),
                                 os.path.basename(binary_input).split(".")[0])

    # Save the data
    if type == IMG_TYPE_AL_MAINSUB:
        pixel_map = np.array(pixel_map)
        main, sub = al.split_mainsub(pixel_map)
        cv.imwrite(output_prefix + "_main.png", main/4)
        cv.imwrite(output_prefix + "_sub.png", sub/4)
        print("Frames saved to {0}_main.png\n".format(output_prefix),
              "and {0}_sub.png".format(output_prefix))
    else:
        cu.save_2d_array_to_png(pixel_map, output_prefix + ".png")
        print("Frame saved to {}.png".format(output_prefix))


def main():
    parser = argparse.ArgumentParser(epilog=textwrap.dedent('''\
         User must include either a type or a bpe (type takes precedence)
         The following types are supported
             al_disparity: 1280x720, 640x360, 320x180
             al_mainsub: 1296x1600
        '''), formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("--type",
                        help="type of input being processed(must be a binary)")
    parser.add_argument("--bpe", type=int,
                        help="number of bytes per element/pixel in image")
    required = parser.add_argument_group('required arguments')
    required.add_argument("--input", help="path of input binary file",
                          required=True)
    required.add_argument("--height", type=int, help="height of the image",
                          required=True)
    required.add_argument("--width", type=int, help="width of the image",
                          required=True)

    args = parser.parse_args()

    # Manual check for malformed arguments
    if args.bpe is None and args.type is None:
        print("Error: Must give either a type or a bpe argument")
        return
    if args.bpe is not None and args.type is not None:
        print("Warning: Both type and bpe found. Program will ingore bpe in",
              "favor of type")

    process(args.input, args.height, args.width, args.type, args.bpe)


if __name__ == "__main__":
    main()
