# !/usr/bin/env python3
import libs.alcam as al
import os
import math
import numpy as np
import matplotlib.pyplot as plt
import cv2
import argparse


class CVcal(object):
    def __init__(self, idx, main_mtx, main_dist, sub_mtx, sub_dist, R, R_angles, T, fx, B, cx, cy, Hfov):
        self.id = idx
        self.main_mtx = main_mtx
        self.sub_mtx = sub_mtx
        self.main_dist = main_dist
        self.sub_dist = sub_dist
        self.R = R
        self.R_angles = R_angles
        self.T = T
        self.fx = fx
        self.B = B
        self.cx = cx
        self.cy = cy
        self.Hfov = Hfov

    def display(self):
        print('\nCamera ID: ', self.id)
        print('Main Matrix:\n', self.main_mtx)
        print('Main Distortion:\n', self.main_dist)
        print('Sub Matrix:\n', self.sub_mtx)
        print('Sub Distortion:\n', self.sub_dist)
        print('Rotational Matrix:\n', self.R)
        print('Rotational Angles (x, y, z):\n', self.R_angles)
        print('Translation Matrix:\n', self.T)
        print('Rectificed Effective Focal Length fx: ', self.fx)
        print('Rectificed Baseline: ', self.B)
        print('Rectificed Principle Offset (cx, cy): ', self.cx, self.cy)
        print('Disparity Map Horizontal FoV (degree): ', self.Hfov)


class IQcal(object):
    def __init__(self, idx, main_bright, sub_bright, main_R_LSC, main_Gr_LSC, main_Gb_LSC, main_B_LSC, sub_R_LSC,
                 sub_Gr_LSC, sub_Gb_LSC, sub_B_LSC):
        self.id = idx
        self.main = main_bright
        self.sub = sub_bright
        self.main_R = main_R_LSC
        self.main_Gr = main_Gr_LSC
        self.main_Gb = main_Gb_LSC
        self.main_B = main_B_LSC
        self.sub_R = sub_R_LSC
        self.sub_Gr = sub_Gr_LSC
        self.sub_Gb = sub_Gb_LSC
        self.sub_B = sub_B_LSC

    def display(self):
        print('\nCamera ID: ', self.id)
        print('Main Bright:\n', self.main)
        print('Sub Bright:\n', self.sub)
        print('Main_R_LSC:\n', self.main_R)
        print('Main_Gr_LSC:\n', self.main_Gr)
        print('Main_Gb_LSC:\n', self.main_Gb)
        print('Main_B_LSC:\n', self.main_B)
        print('Sub_R_LSC:\n', self.sub_R)
        print('Sub_Gr_LSC:\n', self.sub_Gr)
        print('Sub_Gb_LSC:\n', self.sub_Gb)
        print('Sub_B_LSC:\n', self.sub_B)


class CalMain(object):

    def __init__(self, arg):
        self.arg = arg

    def get_files(self, location):
        file_list = [line for line in os.listdir(location)]
        return file_list

    def isRotationMatrix(self, R):
        Rt = np.transpose(R)
        tmp = np.dot(Rt, R)
        I = np.identity(3, dtype=R.dtype)
        n = np.linalg.norm(I - tmp)
        return n < 1e-6

    def rotationMatrixToEulerAngles(self, R):
        assert (self.isRotationMatrix(R))

        sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])
        singular = sy < 1e-6

        if not singular:
            x = math.atan2(R[2, 1], R[2, 2])
            y = math.atan2(-R[2, 0], sy)
            z = math.atan2(R[1, 0], R[0, 0])
        else:
            x = math.atan2(-R[1, 2], R[1, 1])
            y = math.atan2(-R[2, 0], sy)
            z = 0

        return np.array([x, y, z])

    def dataAnalysis(self, data_list, name, title, xlabel, path):
        plt.hist(data_list, bins=10)
        # plt.show()
        plt.title(title)
        plt.xlabel(xlabel)
        plt.savefig(os.path.join(path, name + '_histogram.png'))
        plt.close()
        print('\n>>>> ' + name,
              '\nmean: {} \nmedian: {} \nmax: {} \nmin: {} \nstd: {}:'.format(np.mean(data_list),
                                                                              np.median(data_list), np.max(data_list),
                                                                              np.min(data_list), np.std(data_list)))

    def cv_main(self):
        cv_list = []
        CALDATA_PATH = self.arg.input
        cv_bin_path = CALDATA_PATH + '/cvcal/'
        file_list = self.get_files(cv_bin_path)

        for _file in file_list:
            cam_id = _file.split('_')[0]
            with open(cv_bin_path + _file, 'rb') as cv_bin:
                cvcal = cv_bin.read()
            main_mtx, main_distortion, sub_mtx, sub_distortion, R, T, fx, B, cx, cy = al.get_stereo_cam_cv_params(cvcal, 0)
            angles_r = self.rotationMatrixToEulerAngles(R)
            angles = np.degrees(angles_r)
            Hfov = np.degrees(2 * math.atan2(640, fx))
            # print(fx, B, cx, cy)
            # print(angles_r)
            # print(angles)
            cv = CVcal(cam_id, main_mtx, main_distortion, sub_mtx, sub_distortion, R, angles, T, fx, B, cx, cy, Hfov)
            # if i in [30, 74, 139, 141]:
            if cv.Hfov < 90:
                cv.display()
            else:
                cv_list.append(cv)

        for cv in cv_list:
            # cv.display()
            if cv.fx > 650:
                cv.display()

        B_list = [cv.B for cv in cv_list]
        fx_list = [cv.fx for cv in cv_list]
        cx_list = [cv.cx for cv in cv_list]
        cy_list = [cv.cy for cv in cv_list]
        Tx_list = [cv.T[0, 0] for cv in cv_list]
        Ty_list = [cv.T[1, 0] for cv in cv_list]
        Tz_list = [cv.T[2, 0] for cv in cv_list]
        AngleX_list = [cv.R_angles[0] for cv in cv_list]
        AngleY_list = [cv.R_angles[1] for cv in cv_list]
        AngleZ_list = [cv.R_angles[2] for cv in cv_list]
        Hfov_list = [cv.Hfov for cv in cv_list]
        main_fx_list = [cv.main_mtx[0, 0] for cv in cv_list]
        main_fy_list = [cv.main_mtx[1, 1] for cv in cv_list]
        main_cx_list = [cv.main_mtx[0, 2] for cv in cv_list]
        main_cy_list = [cv.main_mtx[1, 2] for cv in cv_list]
        sub_fx_list = [cv.sub_mtx[0, 0] for cv in cv_list]
        sub_fy_list = [cv.sub_mtx[1, 1] for cv in cv_list]
        sub_cx_list = [cv.sub_mtx[0, 2] for cv in cv_list]
        sub_cy_list = [cv.sub_mtx[1, 2] for cv in cv_list]
        cam_fx_list = main_fx_list + sub_fx_list
        cam_fy_list = main_fy_list + sub_fy_list
        cam_cx_list = main_cx_list + sub_cx_list
        cam_cy_list = main_cy_list + sub_cy_list

        os.makedirs(CALDATA_PATH + '/charts/other', exist_ok=True)
        path = CALDATA_PATH + '/charts/other'

        self.dataAnalysis(B_list, 'Rect_Baseline_mm', 'Stereo Camera Rectified Baseline Histogram', 'baseline (mm)', path)
        self.dataAnalysis(fx_list, 'Rect_EffectiveFocalLen_pix', 'Stereo Camera Rectified Effective Focal Length Histogram',
                          'fx (pixel)', path)
        self.dataAnalysis(cx_list, 'Rect_cx_pix', 'Stereo Camera Rectified Principle Point Offset Histogram', 'cx (pixel)', path)
        self.dataAnalysis(cy_list, 'Rect_cy_pix', 'Stereo Camera Rectified Principle Point Offset Histogram', 'cy (pixel)', path)
        self.dataAnalysis(Hfov_list, 'Horizontal_Fov_degree', 'Stereo Camera Horizontal FOV Histogram',
                          'Horizontal FOV (degree)', path)
        self.dataAnalysis(Tx_list, 'Translation_x_mm', 'Sub to Main Translation', 'x (mm)', path)
        self.dataAnalysis(Ty_list, 'Translation_y_mm', 'Sub to Main Translation', 'y (mm)', path)
        self.dataAnalysis(Tz_list, 'Translation_z_mm', 'Sub to Main Translation', 'z (mm)', path)
        self.dataAnalysis(AngleX_list, 'Rotation_x_degree', 'Sub to Main Rotation', 'angle about x axis (degree)', path)
        self.dataAnalysis(AngleY_list, 'Rotation_y_degree', 'Sub to Main Rotation', 'angle about y axis (degree)', path)
        self.dataAnalysis(AngleZ_list, 'Rotation_z_degree', 'Sub to Main Rotation', 'angle about y axis (degree)', path)

        plt.scatter(cx_list, cy_list)
        plt.title('Stereo Camera Rectified Principle Point Offset')
        plt.xlabel('cx (pixel)')
        plt.ylabel('cy (pixel)')
        plt.savefig(os.path.join(path, 'Rect_offset_scatter.png'))
        plt.close()

        self.dataAnalysis(main_fx_list, 'main_fx_pix', 'Main Camera Effective Focal Length Histogram', 'fx (pixel)', path)
        self.dataAnalysis(main_fy_list, 'main_fy_pix', 'Main Camera Effective Focal Length Histogram', 'fy (pixel)', path)
        self.dataAnalysis(main_cx_list, 'main_cx_pix', 'Main Camera Principle Point Offset Histogram', 'cx (pixel)', path)
        self.dataAnalysis(main_cy_list, 'main_cy_pix', 'Main Camera Principle Point Offset Histogram', 'cy (pixel)', path)
        self.dataAnalysis(sub_fx_list, 'sub_fx_pix', 'Sub Camera Effective Focal Length Histogram', 'fx (pixel)', path)
        self.dataAnalysis(sub_fy_list, 'sub_fy_pix', 'Sub Camera Effective Focal Length Histogram', 'fy (pixel)', path)
        self.dataAnalysis(sub_cx_list, 'sub_cx_pix', 'Sub Camera Principle Point Offset Histogram', 'cx (pixel)', path)
        self.dataAnalysis(sub_cy_list, 'sub_cy_pix', 'Sub Camera Principle Point Offset Histogram', 'cy (pixel)', path)
        self.dataAnalysis(cam_fx_list, 'cam_fx_pix', 'Main/Sub Camera Effective Focal Length Histogram', 'fx (pixel)', path)
        self.dataAnalysis(cam_fy_list, 'cam_fy_pix', 'Main/Sub Camera Effective Focal Length Histogram', 'fy (pixel)', path)
        self.dataAnalysis(cam_cx_list, 'cam_cx_pix', 'Main/Sub Camera Principle Point Offset Histogram', 'cx (pixel)', path)
        self.dataAnalysis(cam_cy_list, 'cam_cy_pix', 'Main/Sub Camera Principle Point Offset Histogram', 'cy (pixel)', path)

        plt.scatter(main_cx_list, main_cy_list)
        plt.title('Main Camera Principle Point Offset')
        plt.xlabel('cx (pixel)')
        plt.ylabel('cy (pixel)')
        plt.savefig(os.path.join(path, 'main_offset_scatter.png'))
        plt.close()

        plt.scatter(sub_cx_list, sub_cy_list)
        plt.title('Sub Camera Principle Point Offset')
        plt.xlabel('cx (pixel)')
        plt.ylabel('cy (pixel)')
        plt.savefig(os.path.join(path, 'sub_offset_scatter.png'))
        plt.close()

        plt.scatter(cam_cx_list, cam_cy_list)
        plt.title('Main/Sub Camera Principle Point Offset')
        plt.xlabel('cx (pixel)')
        plt.ylabel('cy (pixel)')
        plt.savefig(os.path.join(path, 'cam_offset_scatter.png'))
        plt.close()

    def iq_main(self):
        iq_list = []
        CALDATA_PATH = self.arg.input
        iq_bin_path = CALDATA_PATH + '/iqcal/'
        file_list = self.get_files(iq_bin_path)

        for _file in file_list:
            cam_id = _file.split('_')[0]
            with open(iq_bin_path + _file, 'rb') as iq_bin:
                iqcal = iq_bin.read()
            main_bright, sub_bright, main_R_LSC, main_Gr_LSC, main_Gb_LSC, main_B_LSC, sub_R_LSC, sub_Gr_LSC, \
            sub_Gb_LSC, sub_B_LSC = al.get_stereo_cam_iq_params(iqcal)
            iq = IQcal(cam_id, main_bright, sub_bright, main_R_LSC, main_Gr_LSC, main_Gb_LSC, main_B_LSC,
                       sub_R_LSC, sub_Gr_LSC, sub_Gb_LSC, sub_B_LSC)
            iq_list.append(iq)

        x_list = [x for x in range(221)]

        main_R_list = [iq.main[0] for iq in iq_list]
        main_G_list = [iq.main[1] for iq in iq_list]
        main_B_list = [iq.main[2] for iq in iq_list]

        sub_R_list = [iq.sub[0] for iq in iq_list]
        sub_G_list = [iq.sub[1] for iq in iq_list]
        sub_B_list = [iq.sub[2] for iq in iq_list]

        main_R_lsc_list = [iq.main_R for iq in iq_list]
        main_Gb_lsc_list = [iq.main_Gr for iq in iq_list]
        main_Gr_lsc_list = [iq.main_Gb for iq in iq_list]
        main_B_lsc_list = [iq.main_B for iq in iq_list]

        sub_R_lsc_list = [iq.sub_R for iq in iq_list]
        sub_Gb_lsc_list = [iq.sub_Gr for iq in iq_list]
        sub_Gr_lsc_list = [iq.sub_Gb for iq in iq_list]
        sub_B_lsc_list = [iq.sub_B for iq in iq_list]

        lsc_list = ['R_lsc', 'Gb_lsc', 'Gr_lsc', 'B_lsc']
        os.makedirs(CALDATA_PATH + '/charts/main/bright', exist_ok=True)
        os.makedirs(CALDATA_PATH + '/charts/sub/bright', exist_ok=True)
        for grp in lsc_list:
            os.makedirs(CALDATA_PATH + '/charts/main/{}'.format(grp), exist_ok=True)
            os.makedirs(CALDATA_PATH + '/charts/sub/{}'.format(grp), exist_ok=True)

        last = len(file_list)
        for lsc in range(0, last, 5):
            main_dict = {'R_lsc': main_R_lsc_list, 'Gb_lsc': main_Gb_lsc_list, 'Gr_lsc': main_Gr_lsc_list,
                         'B_lsc': main_B_lsc_list}
            for test, val in main_dict.items():
                fig, ax = plt.subplots()
                for xl in range(5):
                    end = lsc + xl
                    ax.scatter(x_list, val[end])
                plt.title('Stereo Main-Camera {} Chart'.format(test))
                plt.xlabel('LSC Point (index)')
                plt.ylabel('LSC Cal (value)')
                plt.savefig(os.path.join(CALDATA_PATH + '/charts/main/{}'.format(test),
                                         'main_cam_{}_{}_scatter.png'.format(test, lsc)))
                plt.close()

            sub_dict = {'R_lsc': sub_R_lsc_list, 'Gb_lsc': sub_Gb_lsc_list, 'Gr_lsc': sub_Gr_lsc_list,
                        'B_lsc': sub_B_lsc_list}
            for test, val in sub_dict.items():
                fig, ax = plt.subplots()
                for xl in range(5):
                    end = lsc + xl
                    ax.scatter(x_list, val[end])
                plt.title('Stereo Sub-Camera {} Chart'.format(test))
                plt.xlabel('LSC Point (index)')
                plt.ylabel('LSC Cal (value)')
                plt.savefig(os.path.join(CALDATA_PATH + '/charts/sub/{}'.format(test),
                                         'sub_cam_{}_{}_scatter.png'.format(test, lsc)))
                plt.close()

        path = CALDATA_PATH + '/charts/main/bright'
        self.dataAnalysis(main_R_list, 'main_cam_R_bright', 'Main Camera R Histogram', 'R (bright)', path)
        self.dataAnalysis(main_G_list, 'main_cam_G_bright', 'Main Camera G Histogram', 'G (bright)', path)
        self.dataAnalysis(main_B_list, 'main_cam_B_bright', 'Main Camera B Histogram', 'B (bright)', path)
        path = CALDATA_PATH + '/charts/sub/bright'
        self.dataAnalysis(sub_R_list, 'sub_cam_R_bright', 'Sub Camera R Histogram', 'R (bright)', path)
        self.dataAnalysis(sub_G_list, 'sub_cam_G_bright', 'Sub Camera G Histogram', 'G (bright)', path)
        self.dataAnalysis(sub_B_list, 'sub_cam_B_bright', 'Sub Camera B Histogram', 'B (bright)', path)


if __name__ == "__main__":
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', "--version", default="IQ", help="Select Method: CV or IQ")
    parser.add_argument('-p', "--input", default="alcam_alpha", help="Path for output files")
    args = parser.parse_args()
    c = CalMain(args)
    if args.version == "IQ":
        c.iq_main()
    else:
        c.cv_main()
