#!/usr/bin/env python3

"""
This module runs regression tests on the AR Altek Stereo Camera

Copyright 2020 Amazon.com, Inc. or its affiliatess. All rights reserved.
"""

import random
import multiprocessing
import time
import git
import shutil as sh
import os
import libs.alcam as al
import libs.common_utils as cu

REMOTE_USERNAME = "root"
REMOTE_IP = "172.17.1.10"
SENSOR_DEV = "/dev/video-ods-cam"

TEMP_WORK_DIR = "alcam-regression-temp"
LIB_ALTEK_REPO = None
LIB_ALTEK_REGRESSION_COMMIT = "2300edd6c99e3f28601b2c6be16a2b5bffc1df96"

TEST_CONFIGURATION_FRAME_RATES = [0, 10, 15, 20, 30]

ALCAM_FIRMWARE_FILES = {"bootfw":   "miniBoot_spinor.bin",
                        "mainfw":   "TBM_SK1.bin",
                        "scenario": "scenario.asb",
                        "irp0":     "IRP0.bin",
                        "irp1":     "IRP1.bin",
                        "hdr":      "HDR.bin",
                        "depth":    "depth.bin"}


def stream_from_camera(config, cam):
    """ Stream from camera with the specified configuration
        Args:
            config (dictionary): altek camera configuration to be tested
            cam (AlcamRemote):   altek camera object being tested
        Returns: 0 if a config passes, -1 otherwise
    """
    if not cam.check_frame_config(config.get("format"),
                                  config.get("width"),
                                  config.get("height"),
                                  config.get("frame_rate")):
        return -1

    name = "{} {}x{}@{}fps:".format(config.get("format"),
                                    config.get("width"),
                                    config.get("height"),
                                    config.get("frame_rate"))
    print("Testing", name, end=' ', flush=True)

    single_shot_mode = (config.get("frame_rate") == 0)
    if single_shot_mode:
        output = multiprocessing.Queue()
        event = multiprocessing.Event()
        event.set()
        single_shot = multiprocessing.Process(target=__activate_trigger,
                                              name="__activate_trigger",
                                              args=(cam, event, output))
        single_shot.start()

    ret = cam.capture("/dev/null", config.get("format"), False,
                      config.get("width"), config.get("height"),
                      config.get("frame_rate"), 200, 1, "")

    success = (ret[0] == 0) and "ERROR: select timeout" not in ret[1]
    if single_shot_mode:
        event.clear()
        single_shot.join()

    if success:
        print("PASS")
        if single_shot_mode:
            output.get()
        return 0
    else:
        print("FAIL")
        print("alcapture output:\n", ret[1], ret[2])

        if single_shot_mode:
            print("Trigger intervals: {}\n".format(output.get()))
        return -1


def __activate_trigger(cam, event, output):
    """ Activates the trigger on the MEA for manual capture
        Args:
             cam (RemoteAlcam): The camera object that will be triggered
             event (multiprocessing.Event): Event used to sync with capture
             output (multiprocessig.Queue): Returns output to main process
    """
    results = []
    count = 0
    time.sleep(1.7)
    while event.is_set():
        cam.trigger()
        if count < 5:
            delay = .04
        else:
            delay = random.randint(33, 2500) / 1000
        time.sleep(delay)
        results.append(delay)
        count += 1
    output.put(results)
    return


def check_streaming(cam):
    """ Verifies that all configurations can be run on the camera
        Args:
             cam (AlcamRemote): altek camera object being tested
        Return: 0 if all test pass, otherwise -1
    """
    # Generate random input and test these inputs
    configs = []
    for rate in TEST_CONFIGURATION_FRAME_RATES:
        for form in cam.formats:
            config = {"format": form.name,
                      "width": form.width,
                      "height": form.height,
                      "frame_rate": rate}
            configs.append(config)
    random.shuffle(configs)

    print('>>>>> Check Streaming:')

    # Individually validate all configurations
    ret = 0
    for config in configs:
        if stream_from_camera(config, cam) < 0:
            ret = -1

    return ret


def check_test_pattern(cam):
    """ Verifies that altest can run on the camera
        Args:
             cam (AlcamRemote): altek camera object being tested
        Return: 0 if the test passes, otherwise -1
    """
    print("\n>>>>> Check Test Pattern:")
    print("Test Pattern {}x{}@{}fps for {} Frames:".format(1280, 720, 30, 200),
          end=' ', flush=True)
    ret = cam.run_altest(30, 200)
    if ret[0] == 0:
        print("PASS")
        return 0
    else:
        print("FAIL")
        print("errnum:{}, errmsg:{}, stderr:{}".format(ret[0], ret[1], ret[2]))
        return -1


def update_all_firmware(cam, fw_directory, expected_versions):
    """ Update all camera firmware to the version on lib-altek's master branch
        Args:
            cam (AlcamRemote): altek camera object being updated
            fw_directory (string): directory housing the firmware
            expected_versions (dict): the expected firmware versions
        Return: 0 if update successful, -1 otherwise
    """
    destination = "/lib/firmware/al6100"
    print("Updating Firmware:")

    # update to the other firware version
    for specific in ALCAM_FIRMWARE_FILES.keys():
        file = "{}/{}".format(fw_directory, ALCAM_FIRMWARE_FILES.get(specific))
        print("Upating {}...".format(specific), end='', flush=True)
        ret_scp = cu.copy_to_remote(cam.username, cam.ip, destination, file)
        ret_echo = cam.update_firmware(specific)

        if ret_scp != 0 or ret_echo[0] != 0:
            print("Failed to update, ending update process")
            return -1
        else:
            print("Done")

    # Reboot the camera to finalize udpate
    print("Rebooting MEA...", end='', flush=True)
    if cam.reboot() == 0:
        print("Done")
    else:
        print("Failed to reconnect to mea after reboot")
        return -1

    print("\nFirmware updated to:")
    actual_firmware = get_firmware_versions_from_camera(cam)
    for fw in ALCAM_FIRMWARE_FILES:
        print((fw + ": " + actual_firmware[fw]).ljust(30), end='')
        if (actual_firmware[fw] != expected_versions[fw]):
            print("FAIL: Expected: " + expected_versions[fw])
        else:
            print('')

    return 0 if actual_firmware == expected_versions else -1


def setup_test():
    """ Do setup work for the regression test
        Return: 0 on success, -1 on failure
    """
    print(">>>>> Setup Test")
    lib_altek_remote = "https://git.kivasystems.com/scm/fir/lib-altek.git"

    # Remove the directory if present from previous regression test
    if os.path.isdir(TEMP_WORK_DIR):
        sh.rmtree(TEMP_WORK_DIR)

    # Create temp directory
    os.mkdir(TEMP_WORK_DIR)

    try:
        print("Cloning lib-altek...", end='')
        os.mkdir(TEMP_WORK_DIR + "/lib-altek")
        global LIB_ALTEK_REPO
        LIB_ALTEK_REPO = git.Repo.clone_from(lib_altek_remote,
                                             TEMP_WORK_DIR + "/lib-altek")

        print("Done")
        return 0
    except Exception:
        print("Fail")
        return -1


def get_firmware_versions_from_README(readme_path):
    """ Gets a dictionary of the expected versions of camera firmware from README
        Args:
            readme_path (string): path to the locally stored binaries/README
        Return: dictionary (firmware name:version), None if error
    """
    expected_versions = open(readme_path).read()
    ret_versions = {}
    for specific in ALCAM_FIRMWARE_FILES.keys():
        target = "{}, version".format(ALCAM_FIRMWARE_FILES.get(specific))
        start = expected_versions.find(target)
        end = expected_versions[start:].find("\n")
        if start < 0 or end < 0:
            print("{} version was not found in README".format(specific))
            return None
        exp_ver = expected_versions[start + len(target) + 1:start + end]
        # check that the readme returns correct version of scenario firmware
        # for this specific iteration of the regression test
        if exp_ver == '7':
            exp_ver = '2.7'
        ret_versions[specific] = exp_ver
    return ret_versions


def get_firmware_versions_from_camera(cam):
    """ Gets a dictionary of the current versions of camera firmware
        Args:
            cam (AltekRemote): Altek camera to be read from
        Return: dictionary (firmware name:version), None if error
    """
    ret_versions = {}
    for specific in ALCAM_FIRMWARE_FILES.keys():
        ret_versions[specific] = cam.get_firmware_version(specific)
        if (ret_versions[specific] is None):
            print("{} version could not be read from camera".format(specific))
            return None
    return ret_versions


def show_system_under_test(cam):
    """ Prints the system running on the MEA
        Args:
        cam (AltekRemote): camera system to display
    """
    fw_versions = get_firmware_versions_from_camera(cam)
    if fw_versions is not None:
        print("\n>>>>> System under Test")
        for version in fw_versions:
            print("{}: {}".format(version, fw_versions[version]))
    else:
        print("Could not read camera firmware versions from mea")


def check_firmware_update(cam):
    """ Check that current camera firmware allows updates other version
        Args:
            cam (AlcamRemote): camera being tested
        Return: 0 if test passed, -1 if test failed
    """
    lib_altek = "{}/al6100_fw".format(TEMP_WORK_DIR + "/lib-altek")
    readme_path = "{}/README".format(lib_altek)
    original_firmware = TEMP_WORK_DIR + "/orig_fw"
    fw_path = "/lib/firmware/al6100"

    # get the original fw versions, copy, and save to orig_fw
    original_versions = get_firmware_versions_from_camera(cam)
    if original_versions is None:
        return -1

    if not os.path.isdir(original_firmware):
        os.mkdir(original_firmware)

    print("\n>>>>> Check Firmware Update")
    print("Saving original camera firmware...", end='')
    for specific in ALCAM_FIRMWARE_FILES.values():
        firmware = fw_path + "/" + specific
        ret = cu.copy_from_remote(cam.username, cam.ip, firmware,
                                  original_firmware)
        if ret != 0:
            print("Fail\nFailed to copy firmware from camera")
            return -1
    print("Done\n")

    # check that other firmware can be updated
    LIB_ALTEK_REPO.git.checkout(LIB_ALTEK_REGRESSION_COMMIT)
    exp_firmware = get_firmware_versions_from_README(readme_path)
    ret_val = update_all_firmware(cam, lib_altek, exp_firmware)
    
    # recover the original fw
    print("\nRecovering Original Firmware")
    if update_all_firmware(cam, original_firmware, original_versions) != 0:
        print("Original firmware not properly recovered")
        ret_val = -1
    else:
        print("Original Firmware Recovered")

    return ret_val


def main():
    """ Runs a series of tests and displays the results
        Return: 0 if all test pass, otherwise -1
    """
    cam = al.AlcamRemote(REMOTE_IP, REMOTE_USERNAME, SENSOR_DEV)

    if setup_test() < 0:
        return -1

    show_system_under_test(cam)

    if check_firmware_update(cam) < 0:
        print("\nFirmware Update Test: FAIL")
        print("Skip the rest of the regression test.")
        print("\n>>>>> Summary\nFAIL")
        return -1
    else:
        print("\nFirmware Update Test: PASS\n")

    results = []

    # Test that the current firmware can stream
    results.append(check_streaming(cam))
    results.append(check_test_pattern(cam))

    # Remove local files
    sh.rmtree(TEMP_WORK_DIR)

    print("\n>>>>> Regression Test Summary")
    if -1 in results:
        print("FAIL")
        return -1
    else:
        print("PASS")
        return 0


if __name__ == "__main__":
    main()
