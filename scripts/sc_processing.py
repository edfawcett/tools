#!/usr/bin/env python3

"""
This module contains API to process AR altek stereo camera data

Copyright 2019 Amazon.com, Inc. or its affiliatess. All rights reserved.
"""

import argparse
import os
import sys
import numpy as np
import cv2
import struct

import libs.alcam as al
import libs.common_utils as cu

# .sc (sensor capture) file format definitions
SC_FILE_HEADER_LEN = 56  # bytes
SC_FILE_HEADER_PREAMBLE = 0x41525353
SC_FILE_VERSIONS = [1]

SC_FRAME_TYPE_AL_DISPARITY_RECT = 1
SC_FRAME_TYPE_AL_MAINSUB_RAW = 2
SC_FRAME_TYPE_DICT = {
    # frame_type: frame type name
    SC_FRAME_TYPE_AL_DISPARITY_RECT: "AR Altek Stereo Camera Disparity Map (rectified)",
    SC_FRAME_TYPE_AL_MAINSUB_RAW: "AR Altek Stereo Camera Main+Sub Raw Image"
}

SC_META_HEADER_LEN = 8  # bytes
SC_META_TYPE_ALCAM_CALIB_IQ = 1
SC_META_TYPE_ALCAM_CALIB_CV = 2
SC_META_TYPE_DICT = {
    # meta_type: {"name": "meta type name", "len": meta_len}
    SC_META_TYPE_ALCAM_CALIB_IQ: {"name": "AR Altek Stereo Camera IQ Calibration Parameters", "len": [3560, 3576]},
    SC_META_TYPE_ALCAM_CALIB_CV: {"name": "AR Altek Stereo Camera CV Calibration Parameters", "len": [824]},
}

SC_MSG_HEADER_LEN = 4    # bytes

def sc_parse_file_header(header):
    """ Parse sc file header
        Args:
            header (str): string of the sc file header
        Returns:
            a list of parameters extracted from the header
    """
    if len(header) != SC_FILE_HEADER_LEN:
        print("Error: invalid input header length", len(header), ". Expected", SC_FILE_HEADER_LEN)
        return None
    preamble, version, frame_type, num_frames, width, height, bits_per_ele, fps, spl_period_sec, \
    rsv0, rsv1, rsv2, rsv3, num_metas \
    = struct.unpack("14I", header)

    if preamble != SC_FILE_HEADER_PREAMBLE:
        print("Error: invalid stream file preamble", preamble)
        return None

    if version not in SC_FILE_VERSIONS:
        print("Error: Unuspported version", version)
        return None

    if frame_type not in SC_FRAME_TYPE_DICT.keys():
        print("Error: unsupported image type", frame_type)
        return None

    print("Frame type:", SC_FRAME_TYPE_DICT[frame_type])
    print("Frame width:", width, "height:", height, "bits per element:", bits_per_ele)
    print("Number of frames:", num_frames)
    print("Frame rate (fps):", fps)
    print("Sampling period (frame count): ", spl_period_sec)
    print("Number of metas:", num_metas)

    return [frame_type, num_frames, width, height, bits_per_ele, num_metas]

def sc_parse_meta_header(meta_hdr):
    """ Parse sc meta header
        Args:
            meta_hdr (str): string of the sc meta header
        Returns:
            meta_type, meta_len (bytes)
    """
    if len(meta_hdr) != SC_META_HEADER_LEN:
        print("Error: invalid input meta header length", len(meta_hdr), ". Expected", SC_META_HEADER_LEN)
        return None

    meta_type, meta_len =  struct.unpack("2I", meta_hdr)
    if not meta_type in SC_META_TYPE_DICT.keys():
        print("Unsupported meta type:", meta_type)
        return None
    if meta_len not in SC_META_TYPE_DICT[meta_type]["len"]:
        print("Invalid meta length:", meta_len, ". Meta type ", meta_type,
            ". expected length: ", SC_META_TYPE_DICT[meta_type]["len"])
        return None
    return meta_type, meta_len

def sc_parse_meta(meta_type, meta, res_height):
    """ Parse sc meta
        Args:
            meta_type (int): sc meta type id
            meta (str): string of the sc meta payload
            res_height (pix): frame resolution, height in pixel
        Returns:
            a list of parameters extracted from the meta
    """
    if len(meta) not in SC_META_TYPE_DICT[meta_type]["len"]:
        print("Error: invalid meta length to parse", len(meta), "Expected ", SC_META_TYPE_DICT[meta_type]["len"])
        return None

    if meta_type == SC_META_TYPE_ALCAM_CALIB_CV:
        return al.get_stereo_cam_cv_params(meta, res_height)
    elif meta_type == SC_META_TYPE_ALCAM_CALIB_IQ:
        return al.get_stereo_cam_iq_params(meta)
    else:
        print("Unsupported meta type:", meta_type)
        return None

def sc_parse_cap_msg_header(msg_hdr):
    """ Parse capture message header
        Args:
            msg_hdr (str): string of the sc message header
        Returns:
            message length (bytes)
    """
    msg_len, = struct.unpack("I", msg_hdr)
    return msg_len

def sc_al_disparity_rect_process(im, cv_calib_params, output_dir, seq, roi,
                    all_pr, disparity, depth, confidence, pts_csv, pts_ply):
    """ Process AR altek stereo camera raw disparity data
        Args:
            im (array): numpy array of raw disparity data
            cv_calib_params (list): a list of calibration params
            output_dir (str): directory to save the output
            seq (int): sequence number of the frame
            roi: [x_min, x_max, y_min, y_max, z_min, z_max], mm
            all, disparity, depth, confidence, pts_csv, pts_ply) (True, False): processing options
    """
    output_prefix = os.path.join(output_dir, 'frame_') + str(seq)

    # get disparity map
    if disparity or all_pr:
        im_disp = al.get_disparity_map(im)
        cu.save_2d_array_to_png(im_disp, output_prefix +"_disp.png")
        #cu.save_2d_array_to_csv(im_disp, output_prefix +"_disp.csv")

    # get confidence map
    if confidence or all_pr:
        im_conf = al.get_confidence_map(im)
        #cu.save_2d_array_to_csv(im_conf, output_prefix +"_conf.csv")

    # get depth map
    if depth or all_pr:
        im_depth = al.get_depth_map(im, cv_calib_params)
        cu.save_2d_array_to_png(im_depth, output_prefix +"_depth.png")
        cu.save_2d_array_to_csv(im_depth, output_prefix +"_depth.csv")

    # get 3D point of cloud
    if pts_csv or pts_ply or all_pr:
        points = al.get_3d_points(im, cv_calib_params, roi)

    # save points as csv file
    if pts_csv or all_pr:
        cu.save_3d_points_to_csv(points, output_prefix +"_points.csv")

    # save points as ply file
    if pts_ply or all_pr:
        cu.save_3d_points_to_ply(points, output_prefix +".ply")

def sc_al_mainsub_process(im, output_dir, seq):
    """ Process AR altek stereo camera main+sub image data
        Args:
            im (array): numpy array of combined image data
            output_dir (str): directory to save the output
            seq (int): sequence number of the frame
    """
    output_prefix = os.path.join(output_dir, 'frame_') + str(seq)
    main, sub = al.split_mainsub(im)
    main_binf = open(output_prefix + "_main.bin", "bw")
    main.tofile(main_binf)
    sub_binf = open(output_prefix + "_sub.bin", "bw")
    sub.tofile(sub_binf)
    cv2.imwrite(output_prefix + "_main.png", main/4)
    cv2.imwrite(output_prefix + "_sub.png", sub/4)
    return

def process(sc_file_path, frame_id, allp, raw, disparity, depth, confidence, pts_csv, pts_ply, roi):
    if sc_file_path is None:
        print("Error: missing input stream file")
        return
    elif not os.path.isfile(sc_file_path):
        print("Error: input file does not exist or is not a file: " + sc_file_path)
        return

    print("Input sensor capture (sc) file: ", sc_file_path)

    if frame_id and frame_id < 0:
        print("Error: invalid frame_id " + str(frame_id))
        return

    output_dir = sc_file_path + "_output"
    if os.path.isfile(output_dir):
        print("Error: Output directory is an existing file: ", output_dir)
        return
    
    # Open input image file
    strm_file = open(sc_file_path, 'rb')

    # Parse sensor capture file header
    header = strm_file.read(SC_FILE_HEADER_LEN)
    fields = sc_parse_file_header(header)
    if fields is None:
        strm_file.close()
        return
    frame_type, num_frames, width, height, bits_per_ele, num_metas = fields

    # Check bits per element
    if (bits_per_ele % 8) > 0:
        print("Error: number of bits per element is not a multiple of 8. Unsupported.")
        strm_file.close()
        return
    
    # Check process frame id
    if frame_id and frame_id >= num_frames:
        print("Error: invalid frame_id", frame_id, ". Valid frame id range: 0 -", num_frames-1)
        strm_file.close()
        return

    # Parse Meta
    total_meta_len = 0
    meta_params = {}  # type:params
    for i in range(num_metas):
        meta_hdr = strm_file.read(SC_META_HEADER_LEN)
        fields = sc_parse_meta_header(meta_hdr)
        if fields is None:
            strm_file.close()
            return
        meta_type, meta_len = fields
        meta = strm_file.read(meta_len)
        params = sc_parse_meta(meta_type, meta, height)
        if params is None:
            strm_file.close()
            return
        meta_params[meta_type] = params
        total_meta_len = total_meta_len + (meta_len + SC_META_HEADER_LEN)

    # Parse Message
    msg_hdr = strm_file.read(SC_MSG_HEADER_LEN)
    msg_len = sc_parse_cap_msg_header(msg_hdr)
    cap_msg = strm_file.read(msg_len).decode('utf-8')
    if len(cap_msg) != msg_len:
        print("Error: failed to read complete capture messasge. " \
            "Expected length ", msg_len, "read ", len(cap_msg))
        strm_file.close()
        return
    print("Capture message: ", cap_msg)
    total_msg_len = SC_MSG_HEADER_LEN + msg_len

    # Check Size
    file_size = os.path.getsize(sc_file_path)
    frame_size = width * height * int(bits_per_ele/8)
    expected_size = SC_FILE_HEADER_LEN + total_meta_len + total_msg_len + num_frames * frame_size
    if file_size != expected_size:
        print("Error: unexpected length. file size ", file_size, ", expected_size ", expected_size)
        strm_file.close()
        return
    
    # frame type specific checks
    if frame_type == SC_FRAME_TYPE_AL_DISPARITY_RECT:
        # check required meta
        if SC_META_TYPE_ALCAM_CALIB_CV not in meta_params.keys():
            print("Error: Missing required meta type: ", SC_META_TYPE_DICT[SC_META_TYPE_ALCAM_CALIB_CV]["name"])
            strm_file.close()
            return

        # verify image size is supported
        supported = al.check_disparity_resolution(width, height)
        if not supported:
            print("Error: invlid disparity size", width, height)
            strm_file.close()
            return

        # get all processing options specified
        processings = (raw, disparity, depth, confidence, pts_csv, pts_ply, all)

        # get region of interest
        print("Region of Interest [x_min, x_max, y_min, y_max, z_min, z_max]", roi)
    elif frame_type == SC_FRAME_TYPE_AL_MAINSUB_RAW:
        processings = [allp]

    if set(processings) == {None}:
        print("No processing option specified")
        strm_file.close()
        return

    # create output directory
    print("Output directory: ", output_dir)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    # process frames
    data_offset = strm_file.tell()
    if frame_id is None:
        fr_ids = range(num_frames)
    else:
        fr_ids = [frame_id]

    for i in fr_ids:
        print("Processing frame", i)
        strm_file.seek(data_offset + i*frame_size)
        raw_data = strm_file.read(frame_size)
        fr = np.frombuffer(raw_data, dtype=np.uint16)
        fr = fr.reshape(height, width)

        # save the raw frame data
        if raw or allp:
            cu.save_to_bin(raw_data, os.path.join(output_dir, 'frame_') + str(i) + '_raw.bin')

        if frame_type == SC_FRAME_TYPE_AL_DISPARITY_RECT:
            sc_al_disparity_rect_process(fr, meta_params[SC_META_TYPE_ALCAM_CALIB_CV], output_dir, i, roi,
                    allp, disparity, depth, confidence, pts_csv, pts_ply)
        elif frame_type == SC_FRAME_TYPE_AL_MAINSUB_RAW:
            sc_al_mainsub_process(fr, output_dir, i)
        else:
            print("Unsupported frame type:", frame_type)

    # close input file
    strm_file.close()

def main():
    """ Process sc file
    """
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--frame_id", type=int, help="sequence number of the frame to be processed, starting from 0")
    parser.add_argument("--all", action="store_true", help="Run all processings applicable")
    parser.add_argument("--raw", action="store_true", help="Save raw frame binary data")
    parser.add_argument("--disparity",action="store_true", help="Save disparity map")
    parser.add_argument("--depth",action="store_true", help="Save disparity map")
    parser.add_argument("--confidence",action="store_true", help="Save confidence map")
    parser.add_argument("--x_min", type=int, help = "Region of interest, minimum value in x (mm)")
    parser.add_argument("--x_max", type=int, help = "Region of interest, maximum value in x (mm)")
    parser.add_argument("--y_min", type=int, help = "Region of interest, minimum value in y (mm)")
    parser.add_argument("--y_max", type=int, help = "Region of interest, maximum value in y (mm)")
    parser.add_argument("--z_min", type=int, help = "Region of interest, minimum value in z (mm)")
    parser.add_argument("--z_max", type=int, help = "Region of interest, maximum value in z (mm)")
    parser.add_argument("--pts_csv", action="store_true", help="Save 3D points to csv file, [x, y, z]")
    parser.add_argument("--pts_ply", action="store_true", help="Save 3D points to ply file")
    required = parser.add_argument_group('required arguments')
    required.add_argument("--input", help="path of input streaming file")

    args = parser.parse_args()
    process(args.input, args.frame_id, args.all, args.raw, args.disparity, args.depth, args.confidence, args.pts_csv, args.pts_ply,
            [args.x_min, args.x_max, args.y_min, args.y_max, args.z_min, args.z_max])


if __name__ == "__main__":
    main()
