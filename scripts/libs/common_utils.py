#!/usr/bin/env python3

"""
This module contains common utility functions

Copyright 2019 Amazon.com, Inc. or its affiliatess. All rights reserved.
"""
import numpy as np
import subprocess as sp
import os
import ctypes
import matplotlib
from matplotlib import pyplot as plt

matplotlib.use("agg")

def save_to_bin(bin_data, output_file):
    """ Save binary data as binary file
        Args:
            bin_data: binary data to be saved
            output_file: path of the output_file
    """
    with open(output_file, "wb") as f:
        f.write(bin_data)

def save_2d_array_to_csv(array, output_file):
    """ Save 2D array to csv file
        Args:
            array: 2d numpy array
            output_file: path of the output_file
    """
    np.savetxt(output_file, array, delimiter=",")

def save_3d_points_to_csv(points, output_file):
    """ Save 3D points to csv file
        Args:
            points: a list of points, each points is represented as [x, y, z]
            output_file: path of the output_file
    """
    with open(output_file, "w") as f:
        for point in points:
            # x, y, z
            f.write(str(point[0]) + ',' + str(point[1]) + ',' + str(point[2]) + '\n')

def save_3d_points_to_ply(points, output_file):
    """ Save 3D points to ply file
        Args:
            points: a list of points, each points is represented as [x, y, z]
            output_file: path of the output_file
    """
    num_points = len(points)
    with open(output_file, "w") as f_ply:
        f_ply.write('ply\n')
        f_ply.write('format ascii 1.0\n')
        f_ply.write('element vertex ' + str(num_points) + '\n')
        f_ply.write('property float x\n')
        f_ply.write('property float y\n')
        f_ply.write('property float z\n')
        f_ply.write('end_header\n')
        for point in points:
            # x, y, z
            f_ply.write(str(point[0]) + ' ' + str(point[1]) + ' ' + str(point[2]) + '\n')

def save_2d_array_to_png(img_map, output_file):
    """ Plot the given data and save with formatted name
        Args:
            img_map(2D array): map of the data to be plotted and saved
            output_file: path of the output file
    """
    plt.ioff()
    plt.imshow(img_map, cmap = plt.get_cmap('jet'))
    plt.colorbar()

    if(output_file[-4:] == ".png"):
        plt.savefig(output_file)
    else:
        plt.savefig(output_file + ".png")

    plt.close()

def __run_subprocess_cmd(cmd):
    """ Run subprocess command
        Args:
            cmd: shell command to run
        Returns:
            error code, stdout msg, stderr msg
    """
    return_code = -1
    if os.name == 'nt':
        old_val = ctypes.c_long()
        # disable win64 file system redirection in order to access C:/Windows/System32
        ret = ctypes.windll.kernel32.Wow64DisableWow64FsRedirection(ctypes.byref(old_val))
        if ret:
            proc = sp.Popen(cmd, stdin=sp.PIPE, stdout = sp.PIPE, stderr = sp.PIPE)
            proc_out, proc_error = proc.communicate()
            return_code = proc.returncode
            # revert file system redirection disable
            ctypes.windll.kernel32.Wow64RevertWow64FsRedirection(old_val)
        else:
            return return_code, "", "failed to disable win64 file system redirection"
    else:
        proc = sp.Popen(cmd, stdin=sp.PIPE, stdout = sp.PIPE, stderr = sp.PIPE)
        proc_out, proc_error = proc.communicate()
        return_code = proc.returncode

    return return_code, proc_out.decode("utf-8"), proc_error.decode("utf-8")

def run_remote_command(host_username, host_ip, command):
    """ Run shell command on remote host
        Args:
            host_username: username to login the remote host
            host_ip: ip address of the remote host
            command: shell command to run on the remote host
        Returns:
            error code, stdout msg, stderr msg
    """
    cmd = ['ssh', '@'.join([host_username, host_ip]), command]
    rets = __run_subprocess_cmd(cmd)
    return rets

def copy_from_remote(host_username, host_ip, host_path, local_path):
    """ Copy from remote host to local
        Args:
            host_username: username to login the remote host
            host_ip: ip address of the remote host
            host_path: path to copy from on the remote host
            local_path: path to copy to on the local machine
        return
            0 for success, other for fail
    """
    cmd = ["scp", "-r", host_username + "@" + host_ip + ":" + host_path, local_path]
    rets = __run_subprocess_cmd(cmd)
    return rets[0]

def copy_to_remote(host_username, host_ip, host_path, local_path):
    """ Copy from remote host to local
        Args:
            host_username: username to login the remote host
            host_ip: ip address of the remote host
            host_path: path to copy to on the remote host
            local_path: path to copy from on the local machine
        return
            0 for success, other for fail
    """
    # enable writing on mea
    run_remote_command(host_username, host_ip, "mount -w -o remount /")

    cmd = ["scp", "-r", local_path, host_username + "@" + host_ip + ":" + host_path]
    rets = __run_subprocess_cmd(cmd)

    # set mea read-only again
    run_remote_command(host_username, host_ip, "mount -r -o remount /")

    return rets[0]
