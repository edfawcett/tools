#!/usr/bin/env python3

"""
This module contains API to process AR altek stereo camera data

Copyright 2019 Amazon.com, Inc. or its affiliatess. All rights reserved.
"""

import numpy as np
import cv2
import struct
import time
import subprocess as sp
from itertools import product
from typing import NamedTuple
import libs.common_utils as cu

FrameFormat = NamedTuple('Format', [('name', str), ('width', int), ('height', int)])


class Alcam:

    def __init__(self):
        """ Create a camera object with constant capture constraints
        """
        self.formats = [FrameFormat("disparity", 1280, 720),
                        FrameFormat("disparity", 640, 360),
                        FrameFormat("disparity", 320, 180),
                        FrameFormat("mainsub", 1296, 1600)]
        self.min_frame_rate = 0
        self.max_frame_rate = 30

    def check_frame_config(self, name, width, height, frame_rate):
        """ Validate disparity map resolution
            Args:
                name (string): name of the capture format
                width (pixel): width of the pixel array
                height (pixel): height of the pixel array
                frame_rate (int): rate of frame capture for camera configuration
            Returns:
                True if the size and frame rate are valid for the given format; False if invalid.
        """
        config = FrameFormat(name, width, height)
        if (config in self.formats and frame_rate in range(self.min_frame_rate, self.max_frame_rate + 1)):
            return True
        else:
            return False


class AlcamRemote(Alcam):

    def __init__(self, remote_ip, remote_username, sensor_dev):
        """ Create a camera running remotely
            Args:
                remote_ip (string): ip address of the mea with this camera
                remote_username (string): username on the mea executing commands
                sensor_dev (string): path to the camera device on mea filesystem
        """
        Alcam.__init__(self)
        self.ip = remote_ip
        self.username = remote_username
        self.dev = sensor_dev

    def capture(self, output, format, testpattern, width, height, rate, count, period, message):
        """ Begin alcapture on this camera with the given configuration
            Args:
                output (string): output directory on mea for stream
                format (string): type of format to be used (eg. mainsub, disparity)
                testpattern (boolean): whether to run a depth pattern test
                width (int): the width of the captured frame in pixels
                height (int): the height of the captured frame in pixels
                rate (int): frame rate of the stream to be run
                count (int): number of frames to be captured
                period (int): save every nth frame where n is the period
                message (string): the message to save the stream with
            Return:
                The output of executing the alcapture on the mea as a tuple (errnum, stdout, stderr)
        """
        command = "/alcam/alcapture "

        if output is not None and len(output) > 0:
            command += "-o {} ".format(output)
        if format is not None and len(format) > 0:
            command += "-f {} ".format(format)
        if testpattern:
            command += "-t "
        if width is not None:
            command += "-W {} ".format(width)
        if height is not None:
            command += "-H {} ".format(height)
        if rate is not None:
            command += "-r {} ".format(rate)
        if count is not None:
            command += "-c {} ".format(count)
        if period is not None:
            command += "-p {} ".format(period)
        if message is not None and len(message) > 0:
            command += "-m {} ".format(message)

        command += "-d {} ".format(self.dev)

        return cu.run_remote_command(self.username, self.ip, command)

    def trigger(self):
        """ Sends signal to capture single frame when capturing in single shot mode
        """
        command = 'echo 1 > /sys/devices/platform/44000000.ocp/48060000.i2c/i2c-2/2-0011/dev_support/trigger'
        return cu.run_remote_command(self.username, self.ip, command)

    def run_altest(self, rate, count):
        """ Runs the altest executable on the remote mea with the given parameters
            Args:
                rate (int): frame rate to be used for the test
                count (int): number of frames to run the test for
            Return:
                The output of executing the altest on the mea as a tuple (errnum, stdout, stderr)
        """
        command = "/alcam/altest -d {} -r {} -c {}".format(self.dev, rate, count)
        return cu.run_remote_command(self.username, self.ip, command)


def check_disparity_resolution(width, height):
    """ Validate disparity map resolution
        Args:
            width (pixel): width of the pixel array
            height (pixel): height of the pixel array
        Returns:
            True if the size is valid; False if invalid.
    """
    valid = ((width == 1280 and height == 720) or
             (width == 640 and height == 360) or
             (width == 320 and height == 180))

    return valid


def get_stereo_cam_cv_params(cal_data, img_height):
    """ Parse AR Altek Sterem Camera CV Calibration Data
        Args:
            cal_data (binary): packed data containing the calibration data binary
            img_height (pixel): height of the image frame
        Returns:
            a list of calibration parameters:
            main_mtx: main camera intrinsics matrix
            main_distortion: main camera distortion coefficients vector
            sub_mtx: sub camera intrinsics matrix
            sub_distortion: sub camera distortion coefficients vector
            fx (float): focal length of stereo camera (pixel)
            B (float): baseline of stereo camera (mm)
            cx (float): principle point offset in x (pixel)
            cy (float): principle point offset in y (pixel)
    """
    # Parse calibration data parameters
    if len(cal_data) != 824:
        print("alcam Error: invalid CV calibration data length", len(cal_data))
        return

    main_fx, main_fy, main_ux, main_uy = struct.unpack_from("4d", cal_data, 8)
    main_distortion = np.array(struct.unpack_from("8d", cal_data, 40))
    main_calib_size = struct.unpack_from("HH", cal_data, 104)
    main_ss_size = struct.unpack_from("HH", cal_data, 108)
    main_cell_size, main_focal_len, main_vcm_slope, main_vcm_offset = struct.unpack_from("ffff", cal_data, 112)

    sub_fx, sub_fy, sub_ux, sub_uy = struct.unpack_from("4d", cal_data, 128)
    sub_distortion = np.array(struct.unpack_from("8d", cal_data, 160))
    sub_calib_size = struct.unpack_from("HH", cal_data, 224)
    sub_ss_size = struct.unpack_from("HH", cal_data, 228)
    sub_cell_size, main_focal_len, main_vcm_slope, main_vcm_offset = struct.unpack_from("ffff", cal_data, 232)

    main_rot = struct.unpack_from("9d", cal_data, 248)
    main_tran = struct.unpack_from("3d", cal_data, 320)

    sub_rot = struct.unpack_from("9d", cal_data, 344)
    R = np.reshape(sub_rot, (3, 3))

    sub_tran = struct.unpack_from("3d", cal_data, 416)
    T = np.reshape(sub_tran, (3, 1))

    main_mtx = np.array([[main_fx, 0, main_ux], [0, main_fy, main_uy], [0, 0, 1]])
    sub_mtx = np.array([[sub_fx, 0, sub_ux], [0, sub_fy, sub_uy], [0, 0, 1]])

    fx, = struct.unpack_from("d", cal_data, 728)
    cx, = struct.unpack_from("d", cal_data, 744)
    cy, = struct.unpack_from("d", cal_data, 752)
    B, = struct.unpack_from("d", cal_data, 760)

    '''
    # scale the parameters for disparity map
    if img_height in [720, 360, 180]:
        # center crop from 1280x800 to 1280x720
        cy = 360 + (cy - 400)

        # scale to lower resolution.
        # base resolution: 1280x720, other resolutions supported: 640x360, 320x180
        scale = 720/img_height
        fx = fx / scale
        cx = cx / scale
        cy = cy / scale

    print("AR Altek Camera Depth Calibration Parameters [fx, B, cx, cy]: ", fx, B, cx, cy)
    '''
    return main_mtx, main_distortion, sub_mtx, sub_distortion, R, T, fx, B, cx, cy


def get_stereo_cam_iq_params(cal_data):
    """ Parse AR Altek Sterem Camera IQ Calibration Data
        Args:
            cal_data (binary): packed data containing the calibration data binary
            img_height (pixel): height of the image frame
        Returns:
            a list of IQ calibration parameters
    """
    # Parse calibration data parameters
    if len(cal_data) != 3560:
        print("alcam Error: invalid IQ calibration data length", len(cal_data))
        return

    main_brightness = struct.unpack_from("HHHHf", cal_data, 0)
    sub_brightness = struct.unpack_from("HHHHf", cal_data, 12)

    main_R_LSC = struct.unpack_from("221H", cal_data, 24)
    main_Gr_LSC = struct.unpack_from("221H", cal_data, 466)
    main_Gb_LSC = struct.unpack_from("221H", cal_data, 908)
    main_B_LSC = struct.unpack_from("221H", cal_data, 1350)

    sub_R_LSC = struct.unpack_from("221H", cal_data, 1792)
    sub_Gr_LSC = struct.unpack_from("221H", cal_data, 2234)
    sub_Gb_LSC = struct.unpack_from("221H", cal_data, 2676)
    sub_B_LSC = struct.unpack_from("221H", cal_data, 3118)

    return main_brightness, sub_brightness, main_R_LSC, main_Gr_LSC, main_Gb_LSC, main_B_LSC, \
           sub_R_LSC, sub_Gr_LSC, sub_Gb_LSC, sub_B_LSC


def get_disparity_map(raw_disp):
    """ Extract disparity map from the raw disparity data
        Args:
            raw_disp (array): numpy array of raw disparity map
        Returns:
            numpy array of disparity map (pixel)
    """
    im_disp = np.copy(raw_disp)
    for x in np.nditer(im_disp, op_flags=['readwrite']):
        x[...] = (x & 0x3ff) / 8.0
    return im_disp


def get_confidence_map(raw_disp):
    """ Extract confidence level map from the raw disparity data
        Args:
            raw_disp (array): numpy array of raw disparity map
        Returns:
            numpy array of confidence level map
    """
    im_conf = np.copy(raw_disp)
    for x in np.nditer(im_conf, op_flags=['readwrite']):
        x[...] = (x & 0xe000) >> 13
    return im_conf


def get_depth_map(raw_disp, calib_params, min_conf=4):
    """ Calculate depth map from the raw disparity data
        Args:
            raw_disp (array): numpy array of raw disparity map
            calib_params: main_mtx, main_dist, sub_mtx, sub_dist, fx, B, cx, cy
            min_conf (int): minimum confidence level to be considered valid, default 4
        Returns:
            numpy array of depth map, mm
    """
    main_mtx, main_dist, sub_mtx, sub_dist, fx, B, cx, cy = calib_params
    im_depth = np.copy(raw_disp)
    for x in np.nditer(im_depth, op_flags=['readwrite']):
        disp = x & 0x3ff
        conf = (x & 0xe000) >> 13
        if disp == 0 or disp == 0x3ff or conf < min_conf:
            x[...] = 0
        else:
            x[...] = fx * B / (disp / 8.0)
    return im_depth


def point_in_roi(point, roi):
    """ Evaluate if a point is in the region of interest
        Args:
            point: [x, y, z], (mm)
            roi: [x_min, x_max, y_min, y_max, z_min, z_max], (mm)
        Returns:
            True if point in ROI, False if not.
    """
    [x, y, z] = point
    [x_min, x_max, y_min, y_max, z_min, z_max] = roi
    if (x_min and x < x_min) or \
            (x_max and x > x_max) or \
            (y_min and y < y_min) or \
            (y_max and y > y_max) or \
            (z_min and z < z_min) or \
            (z_max and z > z_max):
        return False
    else:
        return True


def get_3d_points(raw_disp, calib_params, roi, min_conf=4):
    """ Calculate 3D points from raw disparity map
        Args:
            raw_disp (array): numpy array of raw disparity map
            calib_params: main_mtx, main_dist, sub_mtx, sub_dist, fx, B, cx, cy
            roi: [x_min, x_max, y_min, y_max, z_min, z_max], (mm)
            min_conf (int): minimum confidence level to be considered valid, default 4
        Returns:
            a list of valid points [x, y, z], (mm)
    """
    main_mtx, main_dist, sub_mtx, sub_dist, fx, B, cx, cy = calib_params
    num_rows, num_cols = raw_disp.shape

    ct_zero = 0
    ct_1023 = 0
    ct_valid = 0
    ct_lowconf = 0
    ct_out_roi = 0
    points = []
    for u, v in product(range(num_rows), range(num_cols)):
        disp = raw_disp[u, v] & 0x3ff
        conf = (raw_disp[u, v] & 0xe000) >> 13
        if disp == 0:
            ct_zero += 1
        elif disp == 0x3ff:
            ct_1023 += 1
        elif conf < min_conf:
            ct_lowconf += 1
        else:
            ct_valid += 1
            d = disp / 8.0
            z = fx * B / d
            x = (v - cx) * B / d
            y = (u - cy) * B / d
            p = [x, y, z]
            if point_in_roi(p, roi):
                points.append(p)
            else:
                ct_out_roi += 1

    total = num_rows * num_cols * 1.0
    print("  pixel count disparity=0: %d, %.2f%%" % (ct_zero, (ct_zero / total) * 100))
    print("  pixel count disparity=1023: %d, %.2f%%" % (ct_1023, (ct_1023 / total) * 100))
    print("  pixel count low confidence (< %d): %d, %.2f%%" % (min_conf, ct_lowconf, (ct_lowconf / total) * 100))
    print("  pixel count valid point: %d, %.2f%%" % (ct_valid, (ct_valid / total) * 100))
    print("  pixel count (valid point) out of ROI: %d, %.2f%%" % (ct_out_roi, (ct_out_roi / total) * 100))

    return points


def split_mainsub(raw_image):
    """ Split interlaced main + sub image into separate arrays
        Args:
            raw_image (array): numpy array of raw frame data
        Returns:
            a pair of numpy arrays repesenting each image(empty if error)
    """
    if raw_image[0, -1] != 0x02EF and raw_image[1, -1] != 0x02AB:
        print("ERROR: Invalid padding, main 0x%x, sub 0x%x" % (raw_image[0, -1], raw_image[1, -1]))
        return np.array([]), np.array([])

    sub = raw_image[0::2, :-16]
    main = raw_image[1::2, :-16]
    return main, sub
